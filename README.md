## Obtaining a “Red Hat OpenShift Platform Certified” designation for Corpay application

There are 2 certification formats to acquire for the application:

##   A- Red Hat OpenShift Container certification

The following are the steps needed to complete the Red Hat OpenShift container certification program:

1. Write Dockerfiles to build images that comply with the requirements mentioned [here](https://access.redhat.com/documentation/en-us/red_hat_software_certification/8.53/html/red_hat_openshift_software_certification_policy_guide/assembly-requirements-for-container-images_openshift-sw-cert-policy-introduction) and make sure the base image used in your Dockerfile is a RHEL 7 UBI or RHEL 8 UBI. You may visit the [Red Hat Ecosystem Catalog](https://catalog.redhat.com/software/containers/search) to choose the suitable UBI base image for your container
2. Upload the images to your registry (harbor in our case)
3. Login into your [Red Hat Partner Connect portal](https://connect.redhat.com/) then go to Product Certification tab and choose Manage Certification Projects to set up your container project
4. Click on Create Project and choose as per the following screenshots:

![](/pics/00-18-40.png)

![](/pics/00-20-08.png)

1. Fill in the form as below and make sure to choose "your own container registry" as the distribution method, this is very important.

![](/pics/00-22-38.png)

1. Once completed, click on Create project 

2. Your project will have a Pre certification checklist available under "Overview" tab as shown in the image below

   ![](/pics/09-13-28.png)

   To start with the first item on the Pre certification checklist: "Submit your container for verification" we need to do the following:

3. Create a [pyxis API key](https://connect.redhat.com/account/api-keys) click on Generate New Key then store the key for later use

4. [Running the certification test suite](https://access.redhat.com/documentation/en-us/red_hat_software_certification/8.53/html/red_hat_software_certification_workflow_guide/proc_running-the-certification-test-suite_openshift-sw-cert-workflow-complete-pre-certification-checklist-for-containers) locally by following these steps: 

   - The host machine must have at least RHEL 8.5, CentOS 8.5 or Fedora 35 installed.

   - Install the dependencies:

     ```
     dnf -y install \
         podman \
         buildah \
         jq \
         make \
         bats \
         device-mapper-devel \
         glib2-devel \
         libassuan-devel \
         libseccomp-devel \
         git \
         bzip2 \
         go-md2man \
         runc \
         crun \
         containers-common \
         openscap-containers
         curl -L https://go.dev/dl/go1.19.3.linux-amd64.tar.gz --output go1.19.3.linux-amd64.tar.gz
         rm -rf /usr/local/go && tar -C /usr/local -xzf go1.19.3.linux-amd64.tar.gz
         rm go1.19.3.linux-amd64.tar.gz
         curl -L https://mirror.openshift.com/pub/openshift-v4/clients/oc/latest/linux/oc.tar.gz --output oc.tar.gz
         tar -C /usr/local/bin -xzf oc.tar.gz
         rm oc.tar.gz
         export ARCH=$(case $(uname -m) in x86_64) echo -n amd64 ;; aarch64) echo -n arm64 ;; *) echo -n $(uname -m) ;; esac)
         export OS=$(uname | awk '{print tolower($0)}')
         export OPERATOR_SDK_DL_URL=https://github.com/operator-framework/operator-sdk/releases/download/v1.22.2
         curl -LO ${OPERATOR_SDK_DL_URL}/operator-sdk_${OS}_${ARCH}
         chmod +x operator-sdk_${OS}_${ARCH} && sudo mv operator-sdk_${OS}_${ARCH} /usr/local/bin/operator-sdk    
         echo "PATH=/usr/local/go/bin:$PATH" >> /root/.bashrc
     ```

   - Install the Preflight certification utility tool latest binary from [here](https://github.com/redhat-openshift-ecosystem/openshift-preflight/releases)

   - Run

     ```
     chmod +x preflight*
     sudo mv preflight* /usr/local/bin/preflight
     ```

     

   - Verify that the `preflight` binary can run successfully.

     ```
     preflight --version
     ```

   - Run the test

     ```
     preflight check container harbor.progressoft.io/kryptonite/data-management:221201-4320222a \
     --pyxis-api-token=9efb0olmej0r75g0p9d0rv1xickrd03h \
     --certification-project-id=ospid-638c88958f037f9aec4de7d7 --docker-config=/config.json
     ```

     **Explanation of the command:**

     --pyxis-api-token: is the [pyxis API key](https://connect.redhat.com/account/api-keys)  that was generated in step no. 6

     --certification-project-id: can be found inside the container project under "Overview" tab t

     ![](/pics/09-22-09.png)

     --docker-config: is where you add your authentication for the private registry you pushed the image to. In our case Harbor. The file can be found in ~/.docker/config.json and has the following structure:

     ```
     {
     	"auths": {
     		"harbor.progressoft.io": {
     			"auth": "Tm9xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx=="
     		},
     		"https://index.docker.io/v1/": {
     			"auth": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx="
     		},
     		"registry.redhat.io": {
     			"auth": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
     		}
     	}
     }
     ```

   - Running the test will generate a report. If the test results are PASSED, you can run the following command to submit the results to Red hat

     ```
     preflight check container harbor.progressoft.io/kryptonite/data-management:221201-4320222a \
     --pyxis-api-token=9efb0olmej0r75g0p9d0rv1xickrd03h \
     --submit \
     --certification-project-id=ospid-638c88958f037f9aec4de7d7 --docker-config=/config.json
     ```

   - This is where your image will show up after the submission, inside the container project under "Images" tab: <br />

     ![](/pics/09-27-46.png)


   Now we are done with the first step in the Pre-certification Checklist.

5. The 2nd step is: Accept the Red Hat Container Appendix and accept the terms and conditions

6.  The 3rd step in the Pre-certification Checklist: Provide details about your container

    ![](/pics/09-32-17.png)

    ![](/pics/09-33-03.png)

    

    - Fill out the Container registry namespace: should be the company name (progressoft) in our case

      Note: registry namespace is going to be the same across all projects (containers and helm charts) and this is where your helm project will show in their github repo https://github.com/openshift-helm-charts/charts/tree/main/charts/partners/progressoft

    - Fill out Repository summary, Repository description,  Instruction for users to get your company's image in the Red Hat Container catalog

    - Select standalone image for the Image Type

    - Select the application categories

    - Host level access is Unprivileged

    - Release category is Generally Available

    - Turn off Auto publish until you decide to publish the images

11. 4th step is Attach a completed product listing: for this you need to create a Product Listing first

    - Go to https://connect.redhat.com/manage/projects click on Product Listings tab then click on "Create Listing" and choose Helm Chart.
    - Fill out the necessary Listing details for the product listing

    Note: the last tab "Certification Projects" is where you can attach the container project to your Product Listing.

    You can either do it from this page:

    ![](/pics/16-57-19.png)

    Or from the container project page:

    ![](/pics/16-58-43.png)

    This way your container project will be attached to a helm chart product listing (or container product listing if you don't have a helm chart)

    Note: 

    Product Listings have 2 types: 

    1. Containerized product listing: you can't publish your Containerized Product Listing before publishing your attached container projects first
    2. Helm chart product listing: you can't publish your Helm Product listing before publishing your container projects first, then publishing your helm chart project

12. Publish the containers: go to the Container Project then click on Images tab then click on publish (note: publish button will be disabled if you submitted an image that failed the Preflight test or if your image has security vulnerabilities)

    ![](/pics/17-05-30.png)

    How do I make sure my image doesn't have security vulnerabilities? 

    - Go to your base image listing on redhat catalog (the image you're using as a base image for your dockerfile) for example we used this [one](https://catalog.redhat.com/software/containers/ubi8/openjdk-11/5dd6a4b45a13461646f677f4?container-tabs=security)

    - Click on Security tab

      ![](/pics/09-52-02.png)

    - As you can see the Image has vurlenabilities, to fix this you can either: 

      1. Change the base image

      2. Or add to your dockerfile the following:

         ```
         RUN yum update -y [optional package name list] && \
             yum clean all
         ```

         If your base image doesn't have yum:

         ```
         RUN microdnf upgrade -y [optional package name list] && \
             microdnf clean all
         ```

         



    



## B- Red Hat OpenShift certification for Helm charts <br />

1. Go to https://connect.redhat.com/manage/projects then click on "Create Project" select Red Hat Openshift then Helm Chart

   ![](/pics/10-03-53.png)

2. Fill in the below fields:

   - Project Name (only visible to you) 

   - Chart Name (this name will be visible to the users, a directory with this name will be created in their github and will be added in OWNERS file)
   - For the Distribution Method, select Web Catalog Only
   <br />

   ![](/pics/10-06-42.png)
   <br />

   - Once you create the project, the following will be automatically created in their [github repo under our company registry namespace](https://github.com/openshift-helm-charts/charts/tree/main/charts/partners/progressoft):
     1. A directory will be created with the same name as the one you provided for the "Chart Name"
     2. OWNERS file will be created under the "Chart Name" directory and it will have similar content to:

   ```
   chart:
     name: corpay-progressoft
     shortDescription: "ProgressSoft\u2019s Corporate Banking (PS-CORPay) Suite introduces\
       \ a solution that enables corporates to manage front-office banking services."
   providerDelivery: true
   publicPgpKey: null
   users:
   - githubUsername: nour-alsatari
   vendor:
     label: progressoft
     name: ProgressSoft Corporation
   ```

   Please note: the chart.name in the OWNERS file, is the name you provided when you created the helm chart project and should be the same name in your Chart.yaml (if there's a name mismatch your pull request will fail and not pass their github actions tests)

   Also note: providerDelivery should be set to true, this field is set to true when you choose the Distribution Method as "Web catalog only", if you choose a different distribution method, providerDelivery will be set to false and your helm chart will be publicly available to everyone (more on this later)

3. Complete the Pre-certification Checklist for your Helm Chart Project

   ![](/pics/12-45-43.png)

4. Provide details for your Helm Chart: 

   ![](/pics/12-50-03.png)

     Please note the following:

   - The Container Registry Namespace field should be the same as your container projects Container registry namespace

   - Public PGP key is optional in case you want to sign the submitted Helm chart or report.yaml

   - Authorized GitHub user accounts is for the accounts that can submit the Helm chart through a pull request on github

     P.S: Helm chart submission is not through the website like container projects, Helm chart submission is through their github repo https://github.com/openshift-helm-charts/charts (more on this later)

     

     

5. Check the Helm Chart requirements mentioned [here](https://redhat-connect.gitbook.io/partner-guide-for-red-hat-openshift-and-container/helm-chart-certification/helm-chart-requirements) and make sure your chart is compliant 

   - The chart's apiVersion field must be set to v2: Add this key:value pair to your Chart.yaml <br />"apiVersion: v2" <br /><br />Note: make sure it's added in your parent Chart.yaml and all subcharts Chart.yaml

   - Chart must set the kubeVersion field to indicate the minimum Kubernetes version supported: <br />

     Add this key:value pair to your parent Chart.yaml file only <br />kubeVersion: '> 1.19.0' <br />

   - Chart must include one or more tests, located in the templates directory: <br />

     Create a tests folder in the templates directory of the parent chart. 

     Note: the minimum number of tests you can have is 1 test, and the images used in your tests should be ubi images

     Below are some test samples you can use:

     ```
     apiVersion: v1
     kind: Pod
     metadata:
       name: "{{ .Chart.Name }}-backend-global-test"
       labels:
         app: {{ .Chart.Name }}
         helm.sh/chart: {{ .Chart.Name }}
         app.kubernetes.io/instance: {{ .Release.Name }}
         app.kubernetes.io/managed-by: {{ .Release.Service }}
       annotations:
         "helm.sh/hook": test
         "helm.sh/hook-delete-policy": before-hook-creation,hook-succeeded,hook-failed
     spec:
       containers:
         - name: test
           image: registry.access.redhat.com/ubi8/ubi:8.7-1037
           command: ["/bin/sh"]  
           args: 
             - -c
             - |
               yum -y install nc && \
               nc -zv {{ .Release.Name }}-correspondence 80 && \
               nc -zv {{ .Release.Name }}-corpay-utility 80 && \
               nc -zv {{ .Release.Name }}-checks-mngt 80 && \
               nc -zv {{ .Release.Name }}-payments 80 && \
               nc -zv {{ .Release.Name }}-idm 80 
       restartPolicy: Never
     ```

     ```
     apiVersion: v1
     kind: Pod
     metadata:
       name: "{{ .Chart.Name }}-bank-frontend-shell-global-test"
       labels:
         app: {{ .Chart.Name }}
         helm.sh/chart: {{ .Chart.Name }}
         app.kubernetes.io/instance: {{ .Release.Name }}
         app.kubernetes.io/managed-by: {{ .Release.Service }}
       annotations:
         "helm.sh/hook": test
         "helm.sh/hook-delete-policy": before-hook-creation,hook-succeeded,hook-failed
     spec:
       containers:
         - name: test
           image: registry.access.redhat.com/ubi8/ubi-minimal:8.7-923.1669829893
           command: ["/bin/sh"]  
           args:  ["-c", "microdnf -y install wget && wget bank-{{ .Release.Name }}.{{ .Values.global.VirtualService.baseDomain }}"]
       restartPolicy: Never
     ```

     ```
     apiVersion: v1
     kind: Pod
     metadata:
       name: "{{ .Chart.Name }}-beneficiary-global-test"
       labels:
         app: {{ .Chart.Name }}
         helm.sh/chart: {{ .Chart.Name }}
         app.kubernetes.io/instance: {{ .Release.Name }}
         app.kubernetes.io/managed-by: {{ .Release.Service }}
       annotations:
         "helm.sh/hook": test
         "helm.sh/hook-delete-policy": before-hook-creation,hook-succeeded,hook-failed
     spec:
       containers:
         - name: test
           image: registry.access.redhat.com/ubi8/ubi:8.7-1037
           command: ["/bin/sh"]  
           args:  ["-c", "yum -y install bind-utils && nslookup {{ .Release.Name }}-beneficiary"]
       restartPolicy: Never
     ```

     

   - Chart must include a values.yaml file and a values.schema.json file: <br />

     1- Combine your values in the global Values.yaml

     2- Convert your Values.yaml to json. You can use [this tool](https://www.json2yaml.com/convert-yaml-to-json) to help you with the conversion

     3- Infer a schema.json from your Values.json. You can use [this tool](https://www.jsonschema.net/) to generate the schema.json

     4- Paste the schema into the `values.schema.json` file

     Note: your values.schema.json should be in the same level as the global Values.yaml

     

     What is Values.Schema.Json? <br />

     It's used for input validation. The schema is automatically validated when running the following commands: <br />

     - helm install
     - helm upgrade
     - helm lint
     - helm template

     

   - After adding the Values.Schema.Json, run `helm lint` and make sure your chart passes the lint command

   - Chart must include the charts.openshift.io/name annotation with a human-readable name:

     In the parent chart Chart.yaml file add the following:

    ```
     annotations:
        charts.openshift.io/name: the-same-as-chart-name-in-step-2
        charts.openshift.io/provider: ProgressSoft Corporation
        charts.openshift.io/supportURL: https://www.progressoft.com/
     ```

6. Verify your Helm Chart: To verify your chart and generate the report.yaml you need to install [chart-verifier](https://github.com/redhat-certification/chart-verifier) tool, this tool uses a sub tool called [chart-testing](https://github.com/helm/chart-testing) to install your chart on a cluster your host is connected to

   -  Install the latest version of the [chart-verifier tool](https://github.com/redhat-certification/chart-verifier/releases)

     ```
     wget https://github.com/redhat-certification/chart-verifier/releases/download/${version}/chart-verifier-${version}.tgz
     tar -xf chart-verifier*
     chmod +x chart-verifier
     sudo mv chart-verifier /usr/local/bin
     ```

   - Run the following to confirm the installation: 

     ```
     chart-verifier version
     ```

   - You need to package your Helm Chart into a tarball before running the chart-verifier tool against the chart:

     ```
     helm package <chart-path>
     ```

   -  Run the chart-verifier

     ```
     chart-verifier                                                   \
         verify                                                        \
         --provider-delivery=true                                      \
         --set chart-testing.namespace=test-openshift                  \
         --enable chart-testing                                        \
         corpay-progressoft-19.0.3.tgz
     ```

     The following are helpful arguments that you can use with the chart-verifier tool: <br />

     In case of timeout errors you can set this argument `--timeout  60m0s` as well as this argument `--set chart-testing.helmExtraArgs="--timeout 1200s"` <br />

     To enable any of the chart-verifier checks (sub tests) ` --enable chart-testing` this will run the "chart-testing" check only. <br />

     To  disable any of the chart-verifier checks (sub tests) ` --disable chart-testing` this will run  all the checks (sub tests) except the "chart-testing" check. <br />

     For web-catalog-only distribution method you need to add `--provider-delivery=true` argument.

   - The chart-verifier will generate a report similar to the report.yaml [here](https://github.com/openshift-helm-charts/charts/blob/main/charts/partners/progressoft/corpay-progressoft/19.0.3/report.yaml)

   

7. Submit your chart through a pull request in GitHub: Chart verification is executed via GitHub. Once your Pull Request is approved and merged, this item will be completed.

   - Fork [this repo](https://github.com/openshift-helm-charts/charts) 

   - Clone the forked repo on your host

   - Create a branch "progressoft-${project-name}"

   - cd charts/charts/partners/progressoft/${chart-name} 

   - Create a directory and name it the same as the chart version. Note the version must match the application version in the Chart.yaml `appVersion`

   - Inside this directory, create a report.yaml file and paste the report generated from the chart-verifier in the report.yaml 

     Your helm chart project should have the following tree hierarchy: <br />

     ![](/pics/14-54-21.png)

   - Push your changes to the forked repo

   - Create a pull request from your forked repo to the main branch in this [repo](https://github.com/openshift-helm-charts/charts)

   - Click on the pull request and see the redhat openshift feedback; their repo runs github actions tests against your pull request to validate the report.yaml and the ability to merge. If it passes the github actions they will merge your changes and automatically publish your chart

   - After successful submission and merge, this step in the Pre-certification Checklist will be checked in green <br />

    ![](/pics/15-02-04.png)

     <br />

8. Create a Helm Chart product listing (if you haven't already) 

9. Attach your published helm chart project to it and attach all published containers 

   ![imageedit_2_4116810693](/pics/imageedit_2_4116810693.gif)

10. Publish the Helm Chart listing<br/><br/>

Different Helm Chart project distribution methods & Submission methods: 

**Distribution method:** 

- **Publish your chart in the Red Hat Helm Chart repository**:  Choose this option if you want your chart hosted and published in charts.openshift.io. In this case, users will pull your chart from this repository. Your helm chart will be available in the Ecosystem Catalog

  When people run the below command, they will be able to use your chart because it's public

  ```
  helm repo add openshift-helm-charts https://charts.openshift.io/
  ```

- **Publish your chart in your own Helm Chart repository :** 

  Choose this option if you want to host your helm chart in any publicly available repository. An entry will be added to the index at charts.openshift.io with information about the location of your chart. Users will pull your chart from this repository and your helm chart will be available in the Ecosystem Catalog.

- **Web catalog only** :  Choose this option if you do not want your helm chart to be available via charts.openshift.io. In this case, your helm chart will not be available within the Developer Catalog (or by default with OpenShift) therefore users will not be able to pull your helm chart from this repository. There will only be a catalog listing in the Ecosystem Catalog.

**Submission method:** The submission method depends on the distribution method. 

Please refer to:

 https://github.com/redhat-certification/chart-verifier/blob/main/docs/helm-chart-submission.md

https://redhat-connect.gitbook.io/partner-guide-for-red-hat-openshift-and-container/helm-chart-certification/submitting-your-helm-chart-for-certification/certification-service-options

| Distribution method                                     | Submission method                                            |
| ------------------------------------------------------- | ------------------------------------------------------------ |
| Publish your chart in the Red Hat Helm Chart repository | Submissions should include either a chart or chart and report. |
| Publish you chart in your own Helm Chart repository     | Submissions should be report only using a publicly available chart URL. |
| Web catalog only                                        | This submission should be report only using a private chart URL. |


<br />
<br />

References: 

- https://www.arthurkoziel.com/validate-helm-chart-values-with-json-schemas/

- https://redhat-connect.gitbook.io/partner-guide-for-red-hat-openshift-and-container/helm-chart-certification/creating-a-helm-chart-certification-project

- https://redhat-connect.gitbook.io/partner-guide-for-red-hat-openshift-and-container/helm-chart-certification/verifying-your-helm-chart/running-the-helm-chart-verification-tool

- https://github.com/redhat-certification/chart-verifier/blob/main/docs/helm-chart-submission.md

- https://github.com/openshift-helm-charts/charts/blob/main/docs/README.md#web-catalog-only-delivery



